package quizzes.tests;


import quizzes.domain.Quiz;
import quizzes.repository.Repository;
import quizzes.service.QuizService;
import quizzes.validation.QuizValidator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;

public class QuizServiceTest {

    private QuizService service;

    @Before
    public void setUp(){
        QuizValidator validator= new QuizValidator();
        Repository repo = new Repository(validator);
        service = new QuizService(repo);
    }

    //EP
    @Test
    public void test_valid_maxScoreQuizCounter() {
        service.addQuiz(new Quiz("q1", 10, "Easy", 7));
        service.addQuiz(new Quiz("q2", 15, "Medium", 3));
        service.addQuiz(new Quiz("q3", 9, "Easy", 9));
        service.addQuiz(new Quiz("q4", 5, "Hard", 2));
        service.addQuiz(new Quiz("q5", 20, "Medium", 9));

        assertEquals(2, service.maxScoreQuizCounter());
    }


    //BVA
    @Test
    public void test_Integration_Add_maxScoreQuizCounter(){
        service.addQuiz(new Quiz("q1", 10, "Easy", 7));
        service.addQuiz(new Quiz("q2", 15, "Medium", 3));
        service.addQuiz(new Quiz("q3", 9, "Easy", 9));
        service.addQuiz(new Quiz("q4", 5, "Hard", 2));

        //add check
        assertEquals(4, service.allQuizzes().size());
        //maxScoreQuizCounter
        assertEquals(1, service.maxScoreQuizCounter());
    }

    @After
    public void tearDown(){
        service = null;
    }
}