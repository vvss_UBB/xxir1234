package quizzes.tests;


import quizzes.domain.Quiz;
import quizzes.repository.Repository;
import quizzes.service.QuizService;
import quizzes.validation.QuizValidator;
import org.junit.*;


import static org.junit.Assert.*;

public class QuizServiceTestMaxCounter {
    static QuizService service;

    @Before
    public void setUp() throws Exception {
        QuizValidator validator = new QuizValidator();
        Repository repo = new Repository(validator);
        service = new QuizService(repo);
    }

    @After
    public void tearDown() throws Exception {
        service = null;

    }
    //EC TC1
    @Test
    public void TC1()  {
        service.addQuiz(new Quiz("id_quiz", 10, "Easy", 1));
        assertEquals(1, service.maxScoreQuizCounter());
    }

    //@Ignore
    //EC TC2
    @Test
    public void TC2()  {
        //service.addQuiz(new Quiz("id_quiz", 10, "Easy", 1));
        assertEquals(-1, service.maxScoreQuizCounter());
    }

    @BeforeClass
    public static void setup(){

    }


    @AfterClass
    public static void teardown(){

    }

}