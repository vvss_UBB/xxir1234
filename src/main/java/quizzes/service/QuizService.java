package quizzes.service;

import quizzes.domain.Quiz;
import quizzes.repository.Repository;

import java.util.ArrayList;
import java.util.List;


public class QuizService {

	private Repository repository;


	public QuizService(Repository repository) {
		this.repository = repository;
	}

	public List<Quiz> allQuizzes() {
		return repository.getAll();
	}

    public int maxScore(){
    	int maxScore=0;
    	List<Quiz> quizList = allQuizzes();
		
		for (int i = 0; i < quizList.size(); i++)
			if (quizList.get(i).getCorrectAnswers() > maxScore)
				maxScore = quizList.get(i).getCorrectAnswers();
		
    	return maxScore;
    }

	public List<Quiz> maxScoreQuizList() {
		List<Quiz> quizList = allQuizzes();
		List<Quiz> maxScoreQuizList = new ArrayList<Quiz>();
		int maxScore = maxScore();
		
		for (int i = 0; i < quizList.size(); i++)
			if (quizList.get(i).getCorrectAnswers() == maxScore)
				maxScoreQuizList.add(quizList.get(i));
		
		return maxScoreQuizList;
	}

	//BBT EP + BVA
	//WBT sc, dc, cc, dccc, mcc, apc, lc
	//input: the list of all quizzes
	//output: the number of quizzes that have the maximum no. of correct answers
	public int maxScoreQuizCounter(){
		List<Quiz> quizList = allQuizzes();

		int countMax, index, posMax;
		index=0;countMax=0;posMax=0;
	
		while (index<quizList.size()){
			if (quizList.get(index).getCorrectAnswers() > quizList.get(posMax).getCorrectAnswers()){
				posMax=index;
				countMax=1;
			}
			else
				if (quizList.get(posMax).getCorrectAnswers() == quizList.get(index).getCorrectAnswers())
					countMax++;
			index++;
		}

		if(quizList.size()==0)
			return -1;
		else return countMax;
	}	
	
	public void addQuiz(Quiz quiz) {
		
		//repository.save(quiz);
		repository.add(quiz);
		
	}

}
